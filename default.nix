{ 
  pkgs ? import (fetchTarball https://github.com/NixOS/nixpkgs-channels/archive/6a6e34b81e02fee52a8ba25b4f0a65df36b7c748.tar.gz) {}
}:

with pkgs;
stdenv.mkDerivation rec {
  name = "env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    gmp
    openssl
    libscrypt
    fastpbkdf2
    nlohmann_json
    libbitcoin
    boost
  ];
}

