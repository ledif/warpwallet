#include <iostream>
#include <array>
#include <vector>
#include <string>
#include <cassert>
#include <random>


#include "warp.hpp"
#include "speedometer.hpp"


int main(int argc, char* argv[])
{
  unsigned seed = std::random_device{}();

  if (argc == 2)
    seed = std::atol(argv[1]);

  const char alphabet[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

  constexpr std::size_t alphabet_size = sizeof(alphabet);

  const std::string needle_address = "1MkupVKiCik9iyfnLrJoZLx9RH4rkF3hnA";
  constexpr std::size_t guess_size = 8;

  std::uniform_int_distribution<> dis(0, alphabet_size-2);
  std::mt19937 rng{seed};

  std::string guess(guess_size, 'a');
  const std::string salt = "a@b.c";

  speedometer sped;

  while (true)
  {
    for (auto& x : guess) {
      int r = dis(rng);
      x = alphabet[r];
      assert(x != 0);
    }

    std::cout << "Trying " << guess << std::endl;

    wallet w{guess, salt};

    if (w.address_ == needle_address) {
      std::cout << "FOUND" << std::endl;
      std::cout << w << std::endl;
      return 0;
    }

    sped.tick();
  }

  return 1;
}
