#!/usr/bin/env bash

set -ex

if [ -z "$buildInputs" ]; then
  echo "Run under a nix-shell"
  exit 1
fi

deps=($buildInputs)

openssl=${deps[1]}
libscrypt=${deps[2]}
fastpbkdf2=${deps[3]}
nlohmann_json=${deps[4]}
libbitcoin=${deps[5]}
boost_include=${deps[6]}

boost=$(cat $boost_include/nix-support/propagated-build-inputs | tr -d '[:space:]')
gmp=$(cat ${deps[0]}/nix-support/propagated-build-inputs | tr -d '[:space:]')

make GMP_ROOT=$gmp OPENSSL_ROOT=$openssl LIBSCRYPT_ROOT=$libscrypt FASTPBKDF2_ROOT=$fastpbkdf2 \
     JSON_ROOT=$nlohmann_json LIBBITCOIN_ROOT=$libbitcoin BOOST_ROOT=$boost BOOST_INCLUDE=$boost_include

if [ ! -z $WARPWALLET_BUILD_OMP ]; then
  make GMP_ROOT=$gmp OPENSSL_ROOT=$openssl LIBSCRYPT_ROOT=$libscrypt FASTPBKDF2_ROOT=$fastpbkdf2 \
       JSON_ROOT=$nlohmann_json LIBBITCOIN_ROOT=$libbitcoin BOOST_ROOT=$boost BOOST_INCLUDE=$boost_include \
       challenge_omp
fi

if [ ! -z $WARPWALLET_BUILD_TEST ]; then
  make GMP_ROOT=$gmp OPENSSL_ROOT=$openssl LIBSCRYPT_ROOT=$libscrypt FASTPBKDF2_ROOT=$fastpbkdf2 \
       JSON_ROOT=$nlohmann_json LIBBITCOIN_ROOT=$libbitcoin BOOST_ROOT=$boost BOOST_INCLUDE=$boost_include \
       test_vectors
fi
