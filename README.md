# Warp Wallet Challenge 2

Brute force cracker for the second challenge of Warp Wallet.

## Build

Requires [Nix](https://nixos.org/nix/).

```
nix-shell --pure
./builder.sh
```

All of the requirements (OpenSSL, Boost, libbitcoin, etc.) will be fetched automatically by Nix.

## Usage

To run the single threaded version, simply invoke it:

```
$ ./challenge
```

There is also a multi-threaded version:

```
$ OMP_NUM_THREADS=8 ./challenge_omp
```
