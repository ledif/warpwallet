# FastPBKDF2
FASTPBKDF2_ROOT=/nix/store/nbv2c5v8jdii9xrbc7jjxhjbrmxb9j9h-fastpbkdf2-1.0.0
FASTPBKDF2_INCLUDE_FLAGS=-I$(FASTPBKDF2_ROOT)/include
FASTPBKDF2_LD_FLAGS=$(FASTPBKDF2_ROOT)/lib/libfastpbkdf2.a

# libscrypt
LIBSCRYPT_ROOT=/nix/store/g9j45bmim5yh37m64amkay76j8l43cgp-libscrypt-1.21
LIBSCRYPT_INCLUDE_FLAGS=-I$(LIBSCRYPT_ROOT)/include
LIBSCRYPT_LD_FLAGS=-L$(LIBSCRYPT_ROOT)/lib -lscrypt

# libbitcoin
LIBITCOIN_ROOT=/nix/store/sin5qjw3kl83gjv1n9qikwwz54qy1n1x-libbitcoin-3.4.0
LIBITCOIN_INCLUDE_FLAGS=-I$(LIBITCOIN_ROOT)/include
LIBITCOIN_LD_FLAGS=-L$(LIBITCOIN_ROOT)/lib -lbitcoin

# OpenSSL
OPENSSL_ROOT=/nix/store/asflf95anhvmmmvmcb5r112hgr3yqdp8-openssl-1.1.0f/
OPENSSL_LD_FLAGS=-L$(OPENSSL_ROOT)/lib -lcrypto

# GMP
GMP_ROOT=/nix/store/197xzvy60xrc6hlb1v5s561cqs71mhp7-gmp-6.1.2/
GMP_LD_FLAGS=-L$(GMP_ROOT)/lib -lgmp

# Boost
BOOST_ROOT=/nix/store/4ynxi9r87y9n0vyb141jgjqx8w8rdm0y-boost-1.63.0
BOOST_LD_FLAGS=-L$(BOOST_ROOT)/lib -lboost_system
BOOST_INCLUDE_ROOT=/nix/store/agz1bmkkp3bpj4bd0f4xja6b3xil3rrq-boost-1.65.1-dev
BOOST_INCLUDE_FLAGS=-I$(BOOST_INCLUDE_ROOT)/include

# JSON
JSON_ROOT=/nix/store/rp1am3v9al0ga33ssgy97v0hvghhiayc-nlohmann_json-2.1.0/
JSON_INCLUDE_FLAGS=-I$(JSON_ROOT)/include

# Global include / link flags
INCLUDE_FLAGS=-I./include $(FASTPBKDF2_INCLUDE_FLAGS) $(LIBSCRYPT_INCLUDE_FLAGS) $(LIBITCOIN_INCLUDE_FLAGS) $(JSON_INCLUDE_FLAGS) $(BOOST_INCLUDE_FLAGS)
LDLIBS=$(FASTPBKDF2_LD_FLAGS) $(LIBSCRYPT_LD_FLAGS) $(LIBITCOIN_LD_FLAGS) $(OPENSSL_LD_FLAGS) $(GMP_LD_FLAGS) $(BOOST_LD_FLAGS)

CXXFLAGS += -std=c++11 -O3 -g -Wall $(INCLUDE_FLAGS)

challenge: challenge.cpp
	$(CXX) $(CXXFLAGS) -o challenge challenge.cpp $(LDLIBS)

challenge_omp: challenge_omp.cpp
	$(CXX) $(CXXFLAGS) -fopenmp -o challenge_omp challenge_omp.cpp $(LDLIBS)

test: test_vectors.cpp
	$(CXX) $(CXXFLAGS) -o test_vectors test_vectors.cpp $(LDLIBS)

clean:
	rm -f *.o challenge test_vectors
