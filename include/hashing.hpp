#pragma once

#include <iostream>
#include <array>
#include <vector>
#include <string>

extern "C" {

void fastpbkdf2_hmac_sha256(const uint8_t *pw, size_t npw,
                            const uint8_t *salt, size_t nsalt,
                            uint32_t iterations,
                            uint8_t *out, size_t nout);

int libscrypt_scrypt(const uint8_t *passwd, size_t passwdlen,
        const uint8_t *salt, size_t saltlen, uint64_t N, uint32_t r, 
        uint32_t p, /*@out@*/ uint8_t *buf, size_t buflen);
}

std::array<uint8_t, 32> scrypt(std::vector<uint8_t> guess, std::vector<uint8_t> salt)
{
  constexpr size_t N = 262144;
  constexpr uint32_t r = 8;
  constexpr uint32_t p = 1;

  std::array<uint8_t, 32> buf;

  guess.push_back(1);
  salt.push_back(1);

  libscrypt_scrypt(guess.data(), guess.size(), salt.data(), salt.size(), N, r, p, buf.begin(), buf.size());

  return buf;
}

std::array<uint8_t, 32> pbkdf2(std::vector<uint8_t> guess, std::vector<uint8_t> salt)
{
  constexpr uint32_t c = 65536;

  std::array<uint8_t, 32> buf;

  guess.push_back(2);
  salt.push_back(2);

  fastpbkdf2_hmac_sha256(guess.data(), guess.size(), salt.data(), salt.size(), c, buf.begin(), buf.size());

  return buf;
}


std::array<uint8_t, 32> pbkdf2(std::string guess, std::string salt)
{
  std::vector<uint8_t> guess_ints(guess.size());  
  std::vector<uint8_t> salt_ints(salt.size());  

  std::copy(guess.begin(), guess.end(), guess_ints.begin());
  std::copy(salt.begin(), salt.end(), salt_ints.begin());

  return pbkdf2(std::move(guess_ints), std::move(salt_ints));
}


std::array<uint8_t, 32> scrypt(std::string guess, std::string salt)
{
  std::vector<uint8_t> guess_ints(guess.size());  
  std::vector<uint8_t> salt_ints(salt.size());  

  std::copy(guess.begin(), guess.end(), guess_ints.begin());
  std::copy(salt.begin(), salt.end(), salt_ints.begin());

  return scrypt(std::move(guess_ints), std::move(salt_ints));
}

