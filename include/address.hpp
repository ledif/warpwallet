#pragma once

#include <array>
#include <string>

#include "conversions.hpp"

#include <bitcoin/bitcoin/wallet/payment_address.hpp>
#include <bitcoin/bitcoin/wallet/ec_private.hpp>


std::string create_bitcoin_address(seed_type const& private_key)
{
  libbitcoin::wallet::ec_private priv{private_key, 0x8000, false};  
  return priv.to_payment_address().encoded();
}

std::string wif_of(seed_type const& private_key)
{
  libbitcoin::wallet::ec_private priv{private_key, 0x8000, false};  
  return priv.encoded();
}
