#pragma once

#include <chrono>
#include <iostream>

struct speedometer
{
  speedometer(std::size_t freq = 5)
    : freq_(freq), tick_count_(0)
  {
    start_time_ = std::chrono::system_clock::now();
  }

  void tick()
  {
    tick_count_++;

    if (tick_count_ >= freq_) {
      auto now = std::chrono::system_clock::now();
      double ms = std::chrono::duration_cast<std::chrono::milliseconds>(now - start_time_).count();

      double rate = tick_count_*1000 / ms;
      std::cout << "Guesses per second: " << rate << std::endl;

      start_time_ = now;
      tick_count_ = 0;
    }
  }

  std::size_t freq_;
  std::size_t tick_count_;
  std::chrono::system_clock::time_point start_time_;
};
