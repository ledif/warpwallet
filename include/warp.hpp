#pragma once

#include <string>

#include "conversions.hpp"
#include "hashing.hpp"
#include "address.hpp"

struct wallet
{
  template<typename T, typename U>
  wallet(T const& phrase, U const& salt)
  {
    this->scrypt_ = scrypt(phrase, salt);
    this->pbkdf2_ = pbkdf2(phrase, salt);

    std::transform(scrypt_.begin(), scrypt_.end(), pbkdf2_.begin(), seed_.begin(),
      [](uint8_t a, uint8_t b) { return a ^ b; }
    );

    this->wif_ = wif_of(this->seed_);
    this->address_ = create_bitcoin_address(this->seed_);
  }

  std::array<uint8_t, 32> scrypt_;
  std::array<uint8_t, 32> pbkdf2_;
  std::array<uint8_t, 32> seed_;
  std::string wif_;
  std::string address_;
};

std::ostream& operator<<(std::ostream& os, wallet const& w)
{
  os << to_hex_string(w.scrypt_) << std::endl;
  os << to_hex_string(w.pbkdf2_) << std::endl;
  os << to_hex_string(w.seed_) << std::endl;
  os << w.wif_ << std::endl;
  os << w.address_ << std::endl;
  return os;
}
