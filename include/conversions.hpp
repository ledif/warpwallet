#pragma once

#include <iostream>
#include <array>
#include <string>
#include <cassert>

#include <bitcoin/bitcoin/formats/base_16.hpp>
#include <bitcoin/bitcoin/formats/base_58.hpp>


using seed_type = std::array<uint8_t, 32>;
using public_key_type = std::array<uint8_t, 65>;
using address_type = std::array<char, 98>;

template<typename T, size_t N>
std::ostream& operator<<(std::ostream& os, std::array<T, N> const& str)
{
  for (size_t i = 0; i < N; ++i) os << str[i];
  return os;
}

template<size_t N>
std::string to_hex_string(std::array<uint8_t, N> const& a)
{
  return libbitcoin::encode_base16(a);  
}

template<size_t N>
std::string to_base58(std::array<uint8_t, N> const& a)
{
  return libbitcoin::encode_base58(a);
}