#include <iostream>
#include <fstream>
#include <array>
#include <vector>
#include <string>
#include <cassert>

#include <nlohmann/json.hpp>

#include "warp.hpp"


using json = nlohmann::json;

template<typename T, typename U>
bool expect_eq(T&& t, U&& u)
{
  if (t == u)
    return true;
  else
    std::cout << "Not equal " << t << " != " << u << std::endl;

  return false;
}

int main()
{
  std::ifstream i("test/spec.json");
  json j;
  i >> j;

  auto vectors = j.find("vectors");
  assert(vectors != j.end());

  for (auto v : *vectors) {
    std::string phrase = v["passphrase"];
    std::string salt = v["salt"];

    std::cout << "Testing " << phrase << std::endl;

    wallet w{phrase, salt};

    auto seeds = v["seeds"];
    assert(expect_eq(seeds[0], to_hex_string(w.scrypt_)));
    assert(expect_eq(seeds[1], to_hex_string(w.pbkdf2_)));
    assert(expect_eq(seeds[2], to_hex_string(w.seed_)));

    auto keys = v["keys"];
    std::string priv = keys["private"];
    std::string pub = keys["public"];

    assert(expect_eq(priv, w.wif_));
    assert(expect_eq(pub, w.address_));

    std::cout << priv << "\n" << w.wif_ << "\n";

    std::cout << " ... Passed" << std::endl;;
  }

  return 0;
}
